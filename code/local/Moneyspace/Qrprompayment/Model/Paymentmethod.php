<?php

class Moneyspace_Qrprompayment_Model_Paymentmethod extends Mage_Payment_Model_Method_Abstract {
  

  protected $_code  = 'qrprompayment';

  public function assignData($data)
  {
    $info = $this->getInfoInstance();
     
    return $this;
  }
 
  public function validate()
  {
    parent::validate();
    $info = $this->getInfoInstance();
     
 
    return $this;
  }


  
  public function getOrderPlaceRedirectUrl()
  {
    return Mage::getUrl('qrprompayment/payment/redirect', array('_secure' => false));
  }
}
