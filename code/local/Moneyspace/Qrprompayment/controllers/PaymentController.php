<?php
    
    class Moneyspace_Qrprompayment_PaymentController extends Mage_Core_Controller_Front_Action
    {
        public function gatewayAction()
        {
            
            $order = new Mage_Sales_Model_Order();
            
            $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
            
            
            if (isset($_GET["Ref"]) && $_GET["Ref"] != "") {
                
                
                
                
                $ms_secret_id = Mage::getStoreConfig('payment/qrprompayment/ms_secret_id');
                $ms_secret_key = Mage::getStoreConfig('payment/qrprompayment/ms_secret_key');
                
                $hash = hash_hmac('sha256',$_GET["Ref"].date("YmdHis"),$ms_secret_key);
                
                $payment_data = array(
                                      'secreteID' => $ms_secret_id,
                                      'transactionID' => $_GET["Ref"],
                                      'timeHash' => date("YmdHis"),
                                      'hash' => $hash
                                      );
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v1/findbytransaction/obj");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
                
                $data = $payment_data;
                
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $output = curl_exec($ch);
                $info = curl_getinfo($ch);
                curl_close($ch);
                
                $call_dejson = json_decode($output);
                
                if (!isset($call_dejson[0]->status)){
                    
                    $Amount = "Amount ";
                    $TransactionID = "Transaction ID ";
                    $StatusPayment = "Status Payment ";
                    $Description = "Description  ";
                    
                    if ($call_dejson[0]->$StatusPayment == "Pay Success") {
                        
                        $arr_querystring = array(
                                                 'flag' => 1,
                                                 'orderId' => $orderId
                                                 );
                        Mage_Core_Controller_Varien_Action::_redirect('qrprompayment/payment/response', array('_secure' => false, '_query'=> $arr_querystring));
                    }else{
                        Mage_Core_Controller_Varien_Action::_redirect('qrprompayment/payment/redirect', array('_secure' => false));
                    }
                    
                    
                }
                
                
            }else{
                
                if ($orderId) {
                    
                    Mage_Core_Controller_Varien_Action::_redirect('qrprompayment/payment/redirect', array('_secure' => false));
                    
                }
            }
            
        }
        
        public function redirectAction() // payment process
        {
            
            
            $this->loadLayout();
            $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','qrprompayment',array('template' => 'qrprompayment/moneyspace_qr.phtml'));
            $this->getLayout()->getBlock('content')->append($block);
            $this->renderLayout();
            
        }

        public function qrprocessAction(){

            

            $order_status_payment_ms = Mage::getStoreConfig('payment/qrprompayment/order_status_payment_ms');
            
            
            
            if (isset($_GET["oid"]) && $_GET["oid"] != "") {
                
                $Modelorder = new Mage_Sales_Model_Order();

                preg_match_all('!\d+!', $_GET["oid"], $arroid);
            
                $orderId = $arroid[0][0];
            
                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                
                $ms_secret_id = Mage::getStoreConfig('payment/qrprompayment/ms_secret_id');
                $ms_secret_key = Mage::getStoreConfig('payment/qrprompayment/ms_secret_key');
                
                $hash = hash_hmac('sha256',$_GET["oid"].date("YmdHis"),$ms_secret_key);
                
                $payment_data = array(
                                      'secreteID' => $ms_secret_id,
                                      'orderID' => $_GET["oid"],
                                      'timeHash' => date("YmdHis"),
                                      'hash' => $hash
                                      );
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v1/findbyorderid/obj");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
                $data = $payment_data;
                
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $output = curl_exec($ch);
                $info = curl_getinfo($ch);
                curl_close($ch);

                $call_dejson = json_decode($output);
                
                if (!isset($call_dejson[0]->status)){
                    
                    $Amount = "Amount ";
                    $TransactionID = "Transaction ID ";
                    $StatusPayment = "Status Payment ";
                    $Description = "Description  ";
                    
                    if ($call_dejson[0]->$StatusPayment == "Pay Success") {
                        
                        switch ($order_status_payment_ms) {
                            case "processing":
                                $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
                                break;
                            case "complete":
                                $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                                break;
                            case "closed":
                                $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CLOSED, true)->save();
                                break;
                            case "holded":
                                $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, true)->save();
                                break;
                            default:
                                $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                        }

                        Mage::getSingleton('checkout/session')->unsQuoteId();
                        Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
                    
                    }else if($call_dejson[0]->$StatusPayment == "Pending" || $call_dejson[0]->$StatusPayment == "pending"){

                        $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true)->save();
                        Mage::getSingleton('checkout/session')->unsQuoteId();
                        Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));

                    }else{

                        $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
                        Mage::getSingleton('checkout/session')->unsQuoteId();
                        Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));

                    }
             
                    
                    
                }
                
                echo $output;


                
            }

        }

        public function msupdateAction()
        {
            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
        }
        
        public function responseAction() // return after payment
        {
            if ($this->getRequest()->get("flag") == "1" && $this->getRequest()->get("orderId"))
            {
                $orderId = $this->getRequest()->get("orderId");
                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                $order_status_payment_ms = Mage::getStoreConfig('payment/qrprompayment/order_status_payment_ms');
                
                
                switch ($order_status_payment_ms) {
                    case "processing":
                        $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
                        break;
                    case "complete":
                        $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                        break;
                    case "closed":
                        $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CLOSED, true)->save();
                        break;
                    case "holded":
                        $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, true)->save();
                        break;
                    default:
                        $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                }
                
                
                
                
                Mage::getSingleton('checkout/session')->unsQuoteId();
                Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
                
                
            }
            else
            {
                Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/error', array('_secure'=> false));
            }
            
        }

        public function txerrorAction(){
            $this->loadLayout();
            $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','qrprompayment',array('template' => 'qrprompayment/tx_error.phtml'));
            $this->getLayout()->getBlock('content')->append($block);
            $this->renderLayout();
        }
        
        
    
        
    }
