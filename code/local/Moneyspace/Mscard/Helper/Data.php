<?php

class Moneyspace_Mscard_Helper_Data extends Mage_Core_Helper_Abstract
{
  function getPaymentGatewayUrl() 
  {
    return Mage::getUrl('mscard/payment/gateway', array('_secure' => false));
  }
}