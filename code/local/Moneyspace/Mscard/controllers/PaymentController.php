<?php
    
    class Moneyspace_Mscard_PaymentController extends Mage_Core_Controller_Front_Action
    {
        public function gatewayAction()
        {
            
            $order = new Mage_Sales_Model_Order();
            
            $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
            
            
            if ($orderId) {
                    
                Mage_Core_Controller_Varien_Action::_redirect('mscard/payment/redirect', array('_secure' => false));
                
            }
            
        }
        
        public function redirectAction() // payment process
        {
            
            
            $this->loadLayout();
            $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','mscard',array('template' => 'mscard/moneyspace_payment.phtml'));
            $this->getLayout()->getBlock('content')->append($block);
            $this->renderLayout();
            
        }
        
        public function responseAction() // return after payment
        {

            if ($this->getRequest()->get("flag") == "1" && $this->getRequest()->get("orderId")){
                $orderId = $this->getRequest()->get("orderId");
                $status = $this->getRequest()->get("status");
                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                $order_status_payment_ms = Mage::getStoreConfig('payment/mscard/order_status_payment_ms');


                if ($status && $status == "Pay Success") {
                
                
                    switch ($order_status_payment_ms) {
                        case "processing":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
                            break;
                        case "complete":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                            break;
                        case "closed":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CLOSED, true)->save();
                            break;
                        case "holded":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, true)->save();
                            break;
                        default:
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                    }

                    Mage::getSingleton('checkout/session')->unsQuoteId();
                    Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));

                }else if($status && $status == "Pending" || $status && $status == "pending"){

                    $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true)->save();
                    Mage::getSingleton('checkout/session')->unsQuoteId();
                    Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
                    
                }else{

                    $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
                    Mage::getSingleton('checkout/session')->unsQuoteId();
                    Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/error', array('_secure'=> false));

                }
                
                
                
                
                
                
                
            }else{
                Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/error', array('_secure'=> false));
            }
            
        }

        public function msupdateAction()
        {
            $order = new Mage_Sales_Model_Order();
            
   
            
            
            if (isset($_GET["idpay"]) && $_GET["idpay"] != "" && isset($_GET["oid"]) && $_GET["oid"] != "") {
                
                
                
                $ms_secret_id = Mage::getStoreConfig('payment/mscard/ms_secret_id');
                $ms_secret_key = Mage::getStoreConfig('payment/mscard/ms_secret_key');
                
                $hash = hash_hmac('sha256',$_GET["idpay"].date("YmdHis"),$ms_secret_key);
                
                $payment_data = array(
                                      'secreteID' => $ms_secret_id,
                                      'transactionID' => $_GET["idpay"],
                                      'timeHash' => date("YmdHis"),
                                      'hash' => $hash
                                      );
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v1/findbytransaction/obj");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
                $data = $payment_data;
                
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $output = curl_exec($ch);
                $info = curl_getinfo($ch);
                curl_close($ch);
                
                $call_dejson = json_decode($output);
                
                if (!isset($call_dejson[0]->status)){
                    
                    $Amount = "Amount ";
                    $TransactionID = "Transaction ID ";
                    $StatusPayment = "Status Payment ";
                    $Description = "Description  ";
                    
                    if ($call_dejson[0]->$StatusPayment == "Pay Success") {
                        
                        $arr_querystring = array(
                                                 'flag' => 1,
                                                 'orderId' => $_GET["oid"],
                                                 'status' => $call_dejson[0]->$StatusPayment
                                                 );
                        Mage_Core_Controller_Varien_Action::_redirect('mscard/payment/response', array('_secure' => false, '_query'=> $arr_querystring));
                    
                    }else{

                        $arr_querystring = array(
                            'flag' => 1,
                            'orderId' => $_GET["oid"],
                            'status' => $call_dejson[0]->$StatusPayment
                        );

                        Mage_Core_Controller_Varien_Action::_redirect('mscard/payment/response', array('_secure' => false, '_query'=> $arr_querystring));
                    }
                    
                    
                }
                
                
            }else{
                
                $arr_querystring = array(
                    'flag' => 1,
                    'orderId' => $_GET["oid"],
                    'status' => 'error'
                );

                Mage_Core_Controller_Varien_Action::_redirect('mscard/payment/response', array('_secure' => false, '_query'=> $arr_querystring));
                
            }
        }

        public function txerrorAction(){
            $this->loadLayout();
            $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','mscard',array('template' => 'mscard/tx_error.phtml'));
            $this->getLayout()->getBlock('content')->append($block);
            $this->renderLayout();
        }
        

        
    }
