<?php

class Moneyspace_Mscard_Model_Paymentmethod extends Mage_Payment_Model_Method_Abstract {
  

  protected $_code  = 'mscard';

  public function assignData($data)
  {
    $info = $this->getInfoInstance();
     
    return $this;
  }
 
  public function validate()
  {
    parent::validate();
    $info = $this->getInfoInstance();
     
 
    return $this;
  }


  
  public function getOrderPlaceRedirectUrl()
  {
    return Mage::getUrl('mscard/payment/redirect', array('_secure' => false));
  }
}