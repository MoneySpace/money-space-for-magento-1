<?php

class Moneyspace_Msinstallmentpayment_Helper_Data extends Mage_Core_Helper_Abstract
{
  function getPaymentGatewayUrl() 
  {
    return Mage::getUrl('msinstallmentpayment/payment/gateway', array('_secure' => false));
  }
}