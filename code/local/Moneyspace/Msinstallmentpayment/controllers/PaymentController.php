<?php
    
    class Moneyspace_Msinstallmentpayment_PaymentController extends Mage_Core_Controller_Front_Action
    {
        public function gatewayAction()
        {
            
            $order = new Mage_Sales_Model_Order();
            
            $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
            
            
            if ($orderId) {
                    
                Mage_Core_Controller_Varien_Action::_redirect('msinstallmentpayment/payment/redirect', array('_secure' => false));
                
            }
            
        }
        
        public function redirectAction() // payment process
        {
            
            
            $this->loadLayout();
            $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','msinstallmentpayment',array('template' => 'msinstallmentpayment/installment_select.phtml'));
            $this->getLayout()->getBlock('content')->append($block);
            $this->renderLayout();
            
        }

        public function payprocessAction()
        {



            $selectbank;
            $month;

            if($_POST["selectbank"] == 'KTC'){

                $selectbank = 'KTC';
                $month = $_POST["KTC_months"];

            }else if($_POST["selectbank"] == 'BAY' || $_POST["selectbank"] == 'BAY2' || $_POST["selectbank"] == 'BAY3'){

                $selectbank = 'BAY';
                $month = $_POST["BAY_months"];

            }else if($_POST["selectbank"] == 'FCY'){

                $selectbank = 'FCY';
                $month = $_POST["FCY_months"];

            }





            $order = new Mage_Sales_Model_Order();
    
            $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
    
    
    
            $orderDetails = Mage::getModel('sales/order')->loadByIncrementId($orderId);
    
            $shippingId = $orderDetails->getShippingAddress()->getId();
    
            $address = Mage::getModel('sales/order_address')->load($shippingId);
    
    
            $ms_secret_id = Mage::getStoreConfig('payment/msinstallmentpayment/ms_secret_id');
            $ms_secret_key = Mage::getStoreConfig('payment/msinstallmentpayment/ms_secret_key');
            $feetype = Mage::getStoreConfig('payment/msinstallmentpayment/fee_type');

            $msagreement = Mage::getStoreConfig('payment/msinstallmentpayment/msagreement');
            $installment_ktc = Mage::getStoreConfig('payment/msinstallmentpayment/installment_ktc');
            $installment_bay = Mage::getStoreConfig('payment/msinstallmentpayment/installment_bay');
            $installment_fcy = Mage::getStoreConfig('payment/msinstallmentpayment/installment_fcy');


            $secreteID = $ms_secret_id;
            $secreteKey = $ms_secret_key;
    
            $orderjson = date("YmdHis");
    
            $firstname = $address->firstname;
            $lastname = $address->lastname;
            $email = $address->email;
            $phone = $address->telephone;
            $amount = round($orderDetails["grand_total"],2);
            $currency = 'THB';
            $description = $orderId;
            $address = "";
            $message = "";
            $feeType = $feetype;
            $order_id = $orderjson;
            $gateway_type = "card";
            $successurl = Mage::getBaseUrl().'msinstallmentpayment/payment/msupdate?oid='.$orderId.','.$order_id.'';
            $failurl = Mage::getBaseUrl().'msinstallmentpayment/payment/msupdate?oid='.$orderId.','.$order_id.'';
            $cancelurl = Mage::getBaseUrl().'msinstallmentpayment/payment/msupdate?oid='.$orderId.','.$order_id.'';
    
    
            $timeHash = date("YmdHis");
            $dataHash = hash_hmac('sha256',$firstname . $lastname . $email . $phone . $amount . $currency . $description . $address . $message . $feeType . $timeHash . $order_id . $gateway_type . $successurl . $failurl . $cancelurl, $secreteKey);
    
    
    
            $payment_data = array(
                          'secreteID' => $secreteID,
                          'firstname' => $firstname,
                          'lastname' => $lastname,
                          'email' => $email,
                          'phone' => $phone,
                          'amount' => $amount,
                          'currency' => $currency,
                          'description' => $description,
                          'address' => $address,
                          'message' => $message,
                          'feeType' => $feeType,
                          'customer_order_id' => $order_id,
                          'gatewayType' => $gateway_type,
                          'timeHash' => $timeHash,
                          'hash' => $dataHash,
                          'successUrl' => $successurl,
                          'failUrl' => $failurl,
                          'cancelUrl' => $cancelurl,
                          'agreement' => $msagreement,
                          'bankType' => $selectbank,
                          'startTerm' => "3",
                          'endTerm' => $month,
                          'bgColor' => "#43a047",
                          'txtColor' => "#FFFFFF"
            );
    
    
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v2/createinstallment/obj");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
    
            $data = $payment_data;
    
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
    
            $call_dejson = json_decode($output);
            $TransactionID = "Transaction ID";


            

            if ($call_dejson[0]->status == "Create Success"){

                $arr_querystring = array(
                    'transactionID' => $call_dejson[0]->$TransactionID,
                    'oid' => $order_id,
                    'B' => $selectbank,
                    'M' => $month
                );

                Mage_Core_Controller_Varien_Action::_redirect('msinstallmentpayment/payment/payconfirm', array('_secure' => false, '_query'=> $arr_querystring));
            
            }else if ($call_dejson[0]->status == "Payment amount must be less than 10000"){

                $error = array(
                    'error_text' => urlencode("จำนวนเงินที่ชำระต้องน้อยกว่า 10,000 บาท"),
                );

                Mage_Core_Controller_Varien_Action::_redirect('msinstallmentpayment/payment/txerror', array('_secure' => false, '_query'=> $error));

            }else{

                $error = array(
                    'error_text' => urlencode("กรุณาเลือกการผ่อนชำระอีกครั้ง"),
                );

                Mage_Core_Controller_Varien_Action::_redirect('msinstallmentpayment/payment/txerror', array('_secure' => false, '_query'=> $error));
                
            }


      
            

        }

        public function payconfirmAction()
        {
            

            $tx = $this->getRequest()->get("transactionID");
            $oid = $this->getRequest()->get("oid");
            $B = $this->getRequest()->get("B");
            $M = $this->getRequest()->get("M");

            if(isset($tx) && $tx != "" && isset($oid) && $oid != "" && isset($B) && $B != "" && isset($M) && $M != ""){


                $order = new Mage_Sales_Model_Order();
    
                $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();

                $orderDetails = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                       
                
                $ms_secret_id = Mage::getStoreConfig('payment/msinstallmentpayment/ms_secret_id');
                $ms_secret_key = Mage::getStoreConfig('payment/msinstallmentpayment/ms_secret_key');
    
                $hash = hash_hmac('sha256',$oid.date("YmdHis"),$ms_secret_key);
    
                $payment_data = array(
                                    'secreteID' => $ms_secret_id,
                                    'orderID' => $oid,
                                    'timeHash' => date("YmdHis"),
                                    'hash' => $hash
                );
    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v1/findbyorderid/obj");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
                $data = $payment_data;
    
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $output = curl_exec($ch);
                $info = curl_getinfo($ch);
                curl_close($ch);
    
                $call_dejson = json_decode($output);
    
                if (!isset($call_dejson[0]->status)){                      
        
                    $Amount = "Amount ";
                    $TransactionID = "Transaction ID ";
                    $StatusPayment = "Status Payment ";
                    $Description = "Description ";

                    if ($call_dejson[0]->$Description == $orderId && $call_dejson[0]->$Amount == round($orderDetails["grand_total"],2)){
                   
                        $this->loadLayout();

                        $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','msinstallmentpayment',array('template' => 'msinstallmentpayment/installment_confirm.phtml'));
            
                        $this->getLayout()->getBlock('content')->append($block);
            
                        $this->renderLayout();
                    
                    }else{

                        Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/error', array('_secure'=> false));


                    }
        
  
                }else{
                    Mage_Core_Controller_Varien_Action::_redirect('msinstallmentpayment/payment/payprocess', array('_secure' => false));
                }

            }else{

                Mage_Core_Controller_Varien_Action::_redirect('msinstallmentpayment/payment/txerror', array('_secure' => false));

            }

            
        
        }
        
        public function responseAction() // return after payment
        {

            if ($this->getRequest()->get("flag") == "1" && $this->getRequest()->get("orderId")){
                $orderId = $this->getRequest()->get("orderId");
                $status = $this->getRequest()->get("status");
                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                $order_status_payment_ms = Mage::getStoreConfig('payment/msinstallmentpayment/order_status_payment_ms');


                if ($status && $status == "Pay Success") {
                
                
                    switch ($order_status_payment_ms) {
                        case "processing":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
                            break;
                        case "complete":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                            break;
                        case "closed":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CLOSED, true)->save();
                            break;
                        case "holded":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, true)->save();
                            break;
                        default:
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                    }


                }else{
                    $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true)->save();
                }
                
                
                
                
                Mage::getSingleton('checkout/session')->unsQuoteId();
                Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
                
                
            }else{

                $error = array(
                    'error_text' => urlencode("กรุณาเลือกการผ่อนชำระอีกครั้ง"),
                );

                Mage_Core_Controller_Varien_Action::_redirect('msinstallmentpayment/payment/txerror', array('_secure' => false, '_query'=> $error));
                
            }
            
        }

        public function msupdateAction()
        {


            $oid = explode(',',$_GET["oid"]);

            $ms_secret_id = Mage::getStoreConfig('payment/msinstallmentpayment/ms_secret_id');
            $ms_secret_key = Mage::getStoreConfig('payment/msinstallmentpayment/ms_secret_key');
            $order_status_payment_ms = Mage::getStoreConfig('payment/msinstallmentpayment/order_status_payment_ms');
            
    
            $hash = hash_hmac('sha256',$oid[1].date("YmdHis"),$ms_secret_key);
    
            $payment_data = array(
                                'secreteID' => $ms_secret_id,
                                'orderID' => $oid[1],
                                'timeHash' => date("YmdHis"),
                                'hash' => $hash
            );
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v1/findbyorderid/obj");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
            $data = $payment_data;
    
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
    
            $call_dejson = json_decode($output);
    
            if (!isset($call_dejson[0]->status)){                      
        
                $Amount = "Amount ";
                $TransactionID = "Transaction ID ";
                $StatusPayment = "Status Payment ";
                $Description = "Description ";

                $order = Mage::getModel('sales/order')->loadByIncrementId($call_dejson[0]->$Description);

                if ($call_dejson[0]->$StatusPayment == "Pay Success") {

                    switch ($order_status_payment_ms) {
                        case "pending":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true)->save();
                            break;
                        case "processing":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
                            break;
                        case "complete":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                            break;
                        case "closed":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CLOSED, true)->save();
                            break;
                        case "holded":
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_HOLDED, true)->save();
                            break;
                        default:
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
                    }

                    Mage::getSingleton('checkout/session')->unsQuoteId();
                    Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));


                }else{

                    $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
                    Mage::getSingleton('checkout/session')->unsQuoteId();
                    Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/error', array('_secure'=> false));
                    
                }

                


            }

            
        }

        public function txerrorAction(){
            $this->loadLayout();
            $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','msinstallmentpayment',array('template' => 'msinstallmentpayment/tx_error.phtml'));
            $this->getLayout()->getBlock('content')->append($block);
            $this->renderLayout();
        }
        

        
    }
