<?php


class Moneyspace_Msinstallmentpayment_Model_Fee
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'include',
                'label' => 'ร้านค้ารับผิดชอบดอกเบี้ยรายเดือน',
            ),
            array(
                'value' => 'exclude',
                'label' => 'ผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน ( ดอกเบี้ย : 0.8% , 1% )',
            )
        );
    }


}