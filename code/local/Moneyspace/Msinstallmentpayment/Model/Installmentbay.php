<?php


class Moneyspace_Msinstallmentpayment_Model_Installmentbay
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '0',
                'label' => 'ปิด',
            ),
            array(
                'value' => '3',
                'label' => '3 เดือน',
            ),
            array(
                'value' => '4',
                'label' => '4 เดือน',
            ),
            array(
                'value' => '6',
                'label' => '6 เดือน',
            ),
            array(
                'value' => '9',
                'label' => '9 เดือน',
            ),
            array(
                'value' => '10',
                'label' => '10 เดือน',
            )   
        );
    }


}