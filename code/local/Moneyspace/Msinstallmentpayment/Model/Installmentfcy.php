<?php


class Moneyspace_Msinstallmentpayment_Model_Installmentfcy
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '0',
                'label' => 'ปิด',
            ),
            array(
                'value' => '3',
                'label' => '3 เดือน',
            ),
            array(
                'value' => '4',
                'label' => '4 เดือน',
            ),
            array(
                'value' => '6',
                'label' => '6 เดือน',
            ),
            array(
                'value' => '9',
                'label' => '9 เดือน',
            ),
            array(
                'value' => '10',
                'label' => '10 เดือน',
            ),
            array(
                'value' => '12',
                'label' => '12 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)',
            ),
            array(
                'value' => '18',
                'label' => '18 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)',
            ),
            array(
                'value' => '24',
                'label' => '24 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)',
            ),
            array(
                'value' => '36',
                'label' => '36 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)',
            ), 
        );
    }


}