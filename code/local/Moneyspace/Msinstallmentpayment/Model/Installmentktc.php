<?php


class Moneyspace_Msinstallmentpayment_Model_Installmentktc
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '0',
                'label' => 'ปิด',
            ),
            array(
                'value' => '3',
                'label' => '3 เดือน',
            ),
            array(
                'value' => '4',
                'label' => '4 เดือน',
            ),
            array(
                'value' => '5',
                'label' => '5 เดือน',
            ),
            array(
                'value' => '6',
                'label' => '6 เดือน',
            ),
            array(
                'value' => '7',
                'label' => '7 เดือน',
            ),
            array(
                'value' => '8',
                'label' => '8 เดือน',
            ),
            array(
                'value' => '9',
                'label' => '9 เดือน',
            ),
            array(
                'value' => '10',
                'label' => '10 เดือน',
            )   
        );
    }


}