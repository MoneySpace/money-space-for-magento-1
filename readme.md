
 
# Create Secret ID, Secret Key

  

  

- Login  [https://www.moneyspace.net](https://www.moneyspace.net/)
    
- Choose menu Webhooks

- Enter your domain : http://**yourwebsite**.**com**

- Enter your Webhook http://**yourwebsite**.**com**

  
  
![enter image description here](https://worldwallet.net/img_bitbucket/domain_webhook_url.jpg)
  
![enter image description here](https://worldwallet.net/img_bitbucket/secret_id_key.jpg)


****

# Requirements

- Magento 1+


****

# Installation & Configuration

  

1. Download repository

2. Unzip file

3. Copy the code , design , etc folder and paste it in app folder

4. Choose menu **System** -> **Cache Management**  

5. Click " **Flush Magento cache** " button  

6. Choose menu **System** -> **Configuration** -> **Payment Methods**

7. Choose Moneyspace Payment Method

8. Enter your Secret ID & Secret Key after that save config

  
![enter image description here](https://worldwallet.net/img_bitbucket/magento%201/cache%20menu.jpg)

![enter image description here](https://worldwallet.net/img_bitbucket/magento%201/flush%20cache.jpg)
  
![enter image description here](https://worldwallet.net/img_bitbucket/magento%201/config%20menu.jpg)     ![enter image description here](https://worldwallet.net/img_bitbucket/magento%201/Payment%20methods%20menu.jpg)

![enter image description here](https://worldwallet.net/img_bitbucket/magento%201/ms%20payment.jpg)

  


  

****

# Moneyspace Payment methods

  
- Installment

- QR Code PromptPay

- Pay by Card 3D secured

  
![enter image description here](https://worldwallet.net/img_bitbucket/magento%201/select%20ms%20payment.jpg)



****

  

# Changelog

  
- 2020-10-22 : Updated the order status system.    
- 2020-07-05 : Updated payment methods to light box and fixed bugs.  
- 2020-04-02 : Updated credit card payment and QR Code Promptpay. Added installment payments.
- 2019-07-27 : Added